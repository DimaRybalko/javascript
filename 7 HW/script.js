// 1. Екранування - це заміна символів на їх текстові аналоги
// 2. Функцію можна створити оголосивши ії, тобто - function ім'я функції (аргументи) {тіло} , або 
// за допомогою стрілочки(аргументи) => { тіло }
// 3. Hoisting - це механізм за допомогою якого функції можна використовувати перед їх оголошенням  у коді


function createNewUser() {
    let newUser = {}
    newUser.firstName = prompt("Enter your name");
    newUser.lastName = prompt("Enter your surname");
    newUser.birthday = prompt("Enter your birthday data(dd.mm.yyyy)")

    newUser.getLogin = function () {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }
    
    newUser.getAge = function () {
        let birth = new Date(this.birthday.split('.').reverse().join('.'));
        // let years = new Date().getFullYear() - birth.getFullYear()
        // console.log(years);
        // let months = new Date().getMonth() - birth.getMonth()
        // console.log(months);
        // let days = new Date().getDate() - birth.getDate()
        // console.log(days);
//         if ((months < 0 || months === 0) && days < 0) {
//             return years - 1
//         }  else return years
        let data =Math.floor(((new Date() - birth)/(1000*60*60*24*365))) 
        
         return data
    }      

    
    newUser.getPassword = function () {
        return  this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split(".")[2]
    }
    return newUser
}

let newUser = createNewUser()
console.log(newUser);
let login = newUser.getLogin ()
console.log(login);
let date = newUser.getAge();
console.log(date);
let password = newUser.getPassword()
console.log(password);