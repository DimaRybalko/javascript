// let str1 = "Hello\nWorld";
// let str2 = `Hello
// World`;
// alert(str1 == str2);

// let str = 'Ослик Іа-Іа подивився на віадук';

// let target = 'Іа'; // мета пошуку

// let pos = 0;
// while (true) {
//   let foundPos = str.indexOf(target, pos);
//   if (foundPos == -1) break;

//   alert( `Знайдено тут: ${foundPos}` );
//   pos = foundPos + 1; // продовжуємо з наступної позиції
// }

// let str = "stringify";
// // 'strin', символи від 0 до 5 (не включаючи 5)
// alert(str.slice(0, 20));

// alert("Midget".includes("id", 1))

// alert(`My\n`.length);

// for (let char of "Hello") {
//   alert(char); // H, e, l, l, o (char - спочатку "H", потім "e", потім "l" і т. д.)}

// lert('𝒳'.length); // 2, MATHEMATICAL SCRIPT CAPITAL X
// alert('😂'.length); // 2, FACE WITH TEARS OF JOY
// alert('𩷶'.length);

// let now = new Date();
// alert(now);
// let date1 = new Date("2023-01-18T02:30:45");
// // new Date(year, month, date, hours, minutes, seconds, ms)
// let date = new Date(2023, 0, 18, 2, 30, 45);

// console.log(date1);
// console.log(date);

/**
 * Завдання 1.
 *
 * Запитати у користувача його ім'я.
 * Написати функцію, яка виводить у консоль кількість букв у імені користувача.
 * Обробити кейс, коли на початку слова можуть бути введені пробіли, потрібно обчислити довжину без їхнього обліку.
 * Якщо введено лише цифри (отримане ім'я - це число), вивести в консоль - Помилка, введіть ім'я.
 *
 */

// let getName = prompt("Введіть ваше ім'я")

// function letterCounter() {
//     console.log("довжина", getName.length);
//     console.log("довжина без пробілу", getName.trim().length);
//     if (!isNaN(getName)) console.log("Помилка, введіть ваше ім'я");
// }

// letterCounter(getName)

/**
 * Завдання 2.
 *
 * Написати імплементацію вбудованої функції рядка repeat(times).
 *
 * Функція повинна мати два параметри:
 * - Цільовий рядок, який потрібно повторювати
 * - Кількість повторень цільового рядка.
 *
 * Функція повинна повертати перетворений рядок.
 *
 * Умови:
 * - Генерувати помилку, якщо перший параметр не є рядком, а другий не числом.
 */

// console.log(repeatCustom("currentString", 10));

// function repeatCustom(currentString, number) {
//     if (typeof currentString !== "string" || isNaN(number)) {
//         console.log("error");
//         return
// }

//     let repeatString = "";

//     for (let i = 0; i <= number; i++) {
//         repeatString += currentString;
//     }

//     return repeatString;
// }

// console.log(repeatCustom("string", 10));

// function repeatCustom(string, number) {
//     if (typeof string !== "string" || isNaN(number)) {
//         console.log("Error");
//         return;
//     }

//     let repeatString = "";

//     for (let i = 0; i < number; i++){
//         repeatString += string;
//     }

//     return repeatString;
// }

/**
 * Завдання 3.
 *
 * Написати функцію truncate, яка «обрізає» занадто довгий рядок, додавши в кінці три крапки «...».
 *
 * Функція має два параметри:
 * - Вихідний рядок для обрізки;
 * - Допустима довжина рядка.
 *
 * Якщо рядок довший, ніж його допустима довжина — його необхідно обрізати,
 * і конкатенувати до кінця символ три крапки так,
 * щоб разом з ним довжина рядка дорівнювала максимально допустимої довжині рядка з другого параметра.
 *
 * Якщо рядки не довші, ніж її допустима довжина.
 */

// function truncate(string, maxLength) {

//     if (string.length < maxLength) {
//         return string
//     } else
//     {
//         return string.slice(0, maxLength - 3).concat("...");
//         // let result = string.length - maxLength - 3;
//         //  if (result > maxLength)
//     }

// }
// console.log(truncate("string", 5));

/**
 * Завдання 4.
 *
 * Напишіть функцію, яка приймає рядок як аргумент
 * і перетворює регістр першого символу рядка з нижнього регістра у верхній.
 *
 */

// let text = "sasasadsaf"
// function getArgument(string) {

//    console.log(string[0].toUpperCase() + string.slice(1, string.length));
// }

// getArgument(text)

// let userText = "abcdefgxzcvzxcvzxcvzxcv1";
// function toUpper(str) {
//     console.log(str[0].toUpperCase().concat(str.slice(1, str.length)));
// }
// toUpper(userText);

/**
 * Завдання 1.
 *
 * 1.Створіть масив styles з елементами “Jazz” та “Blues”.
 * 2.Додайте “Rock-n-Roll” в кінець масиву.
 * 3.Замініть значення в середині масиву на “Classics”. Ваш код повинен шукати медіанний елемент у масивах будь-якої довжини.
 * 4.Видаліть перший елемент масиву та покажіть його.
 * 5.Вставте Rap та Reggae на початок масиву.
 */

/* Вигляд масиву по ходу виконання операцій: 
Jazz, Blues
Jazz, Blues, Rock-n-Roll
Jazz, Classics, Rock-n-Roll
Classics, Rock-n-Roll
Rap, Reggae, Classics, Rock-n-Roll
*/

// let style = ["Jazz", "Blues"];
// console.log(style);
// // style.push("Rock-n-Roll")
// style[2] = "Rock-n-Roll"
// console.log(style);
// let middleId = Math.floor(style.length / 2)
// // console.log(middleId);
// // style[middleId] = "Classic"
// style.splice (middleId,1, "Classic")
// console.log(style);
// console.log(style.shift(1));
// console.log(style);
// style.unshift("Rap", "Reggae")
// console.log(style);

/**
 * Завдання 2.
 *
 * Написати функцію я збільшуе єлементи массиву на 1 одиницю, через метод та через циклю
 */

// let arr = [];

// let arr1 = [1, 2, 3, 4, 5]
// for (let element of arr) {
//     arr[element] = arr1[element] + 1;
// }
// console.log(arr);

// console.log(arr.map((element) => element + 1));

// let arr1 = []

// for (let i = 0; i < arr.length; i++) {
//     arr1[i] = arr[i] + 1;
// }
// console.log(arr1);

/**
 * Завдання 3.
 *
 * Написати функцію яка повертає сумму всіх чисел массива
 *
 */

// const arr = [1, 2, 3, 4, 5];
// const arrSecond = [
//   1,
//   "some string",
//   2,
//   "some string",
//   3,
//   "some string",
//   4,
//   "some string",
//   5,
// ];

// console.log(arr.reduce(function (acc, element) {
//     return acc + element
// },
//     0
// ));

// let result = 0;
// for (let i = 0; i < arr.length; i++) {
//  result += arr[i]
// }
// console.log(result);

// function createNewUser() {
//   let newUser = {}; // Створення нового порожнього об'єкта

//   newUser.firstName = prompt("Введіть ваше ім'я:");
//   newUser.lastName = prompt("Введіть ваше прізвище:");

//   newUser.getLogin = function() {
//     let firstLetter = this.firstName.charAt(0).toLowerCase();
//     let lastName = this.lastName.toLowerCase();
//     return firstLetter + lastName;
//   };

//   return newUser;
// }

// // Виклик функції та отримання створеного об'єкта
// let user = createNewUser();

/**
 * Завдання 1.
 *
 * 1.Створіть масив styles з елементами “Jazz” та “Blues”.
 * 2.Додайте “Rock-n-Roll” в кінець масиву.
 * 3.Замініть значення в середині масиву на “Classics”. Ваш код повинен шукати медіанний елемент у масивах будь-якої довжини.
 * 4.Видаліть перший елемент масиву та покажіть його.
 * 5.Вставте Rap та Reggae на початок масиву.
 */

/* Вигляд масиву по ходу виконання операцій: 
Jazz, Blues
Jazz, Blues, Rock-n-Roll
Jazz, Classics, Rock-n-Roll
Classics, Rock-n-Roll
Rap, Reggae, Classics, Rock-n-Roll
*/

// let styles = ['Jazz', 'Blues']
// console.log(styles);

// styles.push('Rock-n-Roll')
// console.log(styles);

// let middleId = Math.floor(styles.length / 2);
// styles[middleId] = "Classic";
// console.log(styles);

// console.log(styles.shift(0));
// console.log(styles);

// styles.unshift("Rap", "Reggae");
// console.log(styles);

/**
 * Завдання 2.
 *
 * Написати функцію я збільшуе єлементи массиву на 1 одиницю, через метод та через циклю
 */

// for (let i = 0; i < result.length; i++) {
//     result[i] = result[i] + 1
// }
// console.log(result);

// console.log(result.map((element) => element+1));
// let arr = [];
// let result = [1,2,5,6];
// for (let element of arr) {
//     arr[element] = result[element] + 1
//   }
//     console.log(arr);

// let arr = [];
// let arr1 = [1, 2, 3, 4, 5]
// for (let element of arr1) {
//     arr = element + 1;
//     console.log(arr);
// }

/**
 * Завдання 3.
 *
 * Написати функцію яка повертає сумму всіх чисел массива
 *
 */

// const arr = [1, 2, 3, 4, 5];
// const arrSecond = [
//   1,
//   "some string",
//   2,
//   "some string",
//   3,
//   "some string",
//   4,
//   "some string",
//   5,
// ];

// console.log(arr.reduce(function (acc, number) {
//  return acc + number
// }, 0));

// let result = 0;
// for (let i = 0; i < arr.length; i++) {
//     result += arr[i]
// }
// console.log(result);

/*
 * Завдання 1.
 *
 * Створити масив об'єктів students (у кількості 7 шт).
 * У кожного студента має бути ім'я, прізвище та напрямок навчання - Full-stask чи Front-end.
 * Кожен студент повинен мати метод, sayHi, який повертає рядок `Привіт, я ${ім'я}, студент Dan IT, напрям ${напрямок}`.
 * Перебрати кожен об'єкт і викликати у кожного об'єкта метод sayHi;
 *
 */

// function createStudents(userCount) {
//     let users = [];
//     for (let i = 0; i < userCount; i++) {
//         users[i] =   {
//         name: `Andrei ${i}` ,
//         surname: "Antonov",
//             speciality: i % 2 === 0 ? "Full-stask":"Front-end",
//         sayHi() { alert(`Привіт, я ${this.name}, студент Dan IT, напрям ${this.speciality}`)

//       }
//     }
//     } return users

// }
// let result = createStudents(7);
// result.forEach((student) => student.sayHi())

/*
 * Завдання 2.
 *
 * Є масив брендів автомобілів ["bMw", "Audi", "teSLa", "toYOTA"].
 * Вам потрібно отримати новий масив, об'єктів типу
 * { type: 'car', brand: ${елемент масиву} }
 *
 * Вивести масив у консоль
 */

// let auto = ["bMw", "Audi", "teSLa", "toYOTA"]

/*
 * Завдання 3.
 *
 * Створити масив чисел від 1 до 100.
 * Відфільтрувати його таким чином, щоб до нового масиву не потрапили числа менше 10 і більше 50.
 * Вивести у консоль новий масив.
 *
 */

// function grow (number) {
//     let users = [];
//     for (let i = 0; i < number ; i++) {
//         users[i] = i + 1;
//     }
//     return users
// }
// let numbers = grow(100);
// console.log(numbers);

// let result = numbers.filter((element) => { return element > 10 && element < 50 })
// console.log(result);

// let summation = function (num) {
//     let result = 0;
//     for (let i = 1; i <= num; i++) {
//     result += i
//     } return result

// }

// console.log(summation(10));

// function getSum(a, b)
// {
//     let result = 0;
//     if (a < b) {
//         for (let i = a; i <= b; i++) {
//             result += i
//         }
//     } else {
//         for (let i = b; i <= a; i++) {
//             result += i
//         }
//     } return result
// }
// console.log(getSum(0, -1)); -1 + 0 + 1 + 2 + 3 + 4 + 5

// function getSum(a, b)
// { let result = 0;
//  if (a < b) {
//   for (let i = a; i <= b; i++) {
//    result += i
//      }}
//  else for (let i = b; i <= a; i++) {
//    result += i
//    return result
//   }

// function rentalCarCost(d) {
//   let cost = 40;
//     if (d >= 1 && d <= 3) {
//     return cost*d - 20
//   } else if (d>3 && d<=7) {
//     return cost*d-50
//   }
// }

// console.log(rentalCarCost(4));
// function greet() {
//   return "hello world!"
// }
// console.log(greet()); 0,25 0,14 0,1 0,7

// function SeriesSum(n)
// {   let result = 0
//     for (let i = 1; i <= n*3 ; i += 3) {
//     result += 1/i
//   } return result.toFixed(2)
// }
// console.log(SeriesSum(4));

// function SeriesSum(n)
// {   let result = 0;
//     for (let i = 0; i <= n ; i++) {
//     result +=  1/(3*i+1)
//   } return result.toFixed(2)
// }
// console.log(SeriesSum(5))

// **
//  * Завдання 1.
//  *
//  * Який буде результат обчислення?
//  *
// let a = "" + 1 + 0;
// console.log(a);
// console.log("" - 1 + 0);
// let a = true + false
//  console.log(a)
// let a = 6 / "3"
//  console.log(a)
// let a = "2" * "3"

//  let a = 4 + 5 + "px"
// console.log(a)
//   console.log("$" + 4 + 5);
//   console.log("4" - 2);
//   console.log("4px" - 2 );
//   console.log("  -9  " + 5);
//  console.log("  -9  " - 5);
//  console.log(null + 1);
//  console.log(undefined + 1);
// //  *
//  *

/**
 * Завдання 2.
 *
 * Використовуючи if..else, напишіть код, що отримує число за допомогою prompt і потім виводить повідомлення alert:
 * 1, якщо значення більше нуля,
 * -1, якщо меньше нуля,
 * 0, якщо дорівнює нулю.
 * У цьому завданні ми припускаємо, що введенне значення завжди є числом.
 *
 */

// let value = prompt("Enter some value");
// if (value >= 1) {
//   alert("1")
// } else if (value == 0) {
//   alert("0")
// } else alert("-1")

/**
 * Завдання 3.
 *
 * Перепишіть if..else, використовуючи декілька тернарних операторів '?'.
 *
 */

// const login = "Директор"
// const login = ""
// const login = "Працівник";
// ;

// let message = (login == "Працівник") ?  message = "Привіт" : message = "";
// //  let message = (login == "Директор") ? message = "Вітаю" : message = "";
// // let message = (login == "") ? message = "Немає логіну" : message = "";

// console.log(message);

// if (login == "Працівник") {
//   message = "Привіт";
// } else if (login == "Директор") {
//   message = "Вітаю";
// } else if (login == "") {
//   message = "Немає логіну";
// } else {
//   message = "";
// }

/**
 * Завдання 4.
 *
 * За допомогою циклу вивести в консоль всі непарні числа,
 * які перебувають у діапазоні від 0 до 100.
 *
 */

// function counter (start, end) {
//   for (let i = start; i <= end; i++) {
// if ( i % 2 === 1) console.log(i);
// }

// }
// counter(10,20)

/**
 * Завдання 5.
 *
 * Написати програму-калькулятор.
 *
 * Програма запитує у користувача три значення:
 * - Перше число;
 * - Друге число;
 * - Математична операція, яку необхідно здійснити над введеними числами.
 *
 * Програма повинна повторно запитувати дані, якщо:
 * - Будь-яке необхідних чисел не відповідають критерію коректного числа;
 * - Математична операція не є однією з: +, -, /, *, **.
 *
 * Якщо всі дані введені правильно, програма обчислює зазначену операцію, і виводить у консоль результат:
 * «Над числами ЧИСЛО_1 і ЧІСЛО_2 була проведена операція ОПЕРАЦІЯ. Результат: РЕЗУЛЬТАТ.
 *
 * Після першого успішного виконання програма повинна запитати, чи є необхідність виконатися ще раз,
 * і повинна починати свою роботу спочатку доти, доки користувач не відповість "Ні.".
 *
 * Коли користувач відмовиться продовжувати роботу програми, програма виводить повідомлення:
 * «✅ Робота завершена.».
 */

/**
 * Завдання 6.
 *
 * Користувач вводить 3 числа.
 * Вивести в консоль повідомлення з максимальним числом із введених.
 *
 * Якщо одне із введених користувачем чисел не є числом,
 * вивести повідомлення: «⛔️ Помилка! Одне з введених чисел не є числом..
 *
 */

/*
 * Завдання 7.
 *
 *  Створити об'єкт danItStudent,
 * У якого такі властивості: ім'я, прізвище, кількість зданих домашніх работ.
 *
 * Запитати у користувача "Що ви хочете дізнатися про студента?"
 * Якщо користувач запровадив " name " чи " ім'я " - вивести у консоль ім'я студента.
 * Якщо користувач ввів "lastName" або "прізвище" - вивести до консольпрізвища студента.
 * Якщо користувач запровадив " оцінка " чи " homeworks " - вивести у консоль кількість зданих робіт.
 * Якщо ввів, щось не з перерахованого - вивести в консоль - "Вибачте таких даних немає"
 */

// const danItStudent = {
//   name: "Sanya",
//   lastName: "Vegan",
//   homeworks: "7",

// }

// function chooser() {
//   let question = prompt("Що ви хочете дізнатися про студента?")
//     if (danItStudent[question]) {
//     console.log(danItStudent[question]);
//   } else console.log("Вибачте таких даних немає");
// }

// chooser();

/*
 * Завдання 10.
 *
 * Напишіть функцію, яка очищає масив від небажаних значень,
 *
 * Таких як false, undefined, порожні рядки, нуль, null.
 *
 */

// const data = [0, 1, false, 2, undefined, '', 3, null];
// console.log(compact(data)) // [1, 2, 3]

// function cleaner(arr, type) {
//   let newArray = [];
//   for (let element of arr) {
//     if (typeof element == type) {
//     newArray.push(element)
//     } else if (element == 0) {

//   }
// } return newArray
// }
// let array = [0, 1, false, 2, undefined, '', 3, null]
// console.log(cleaner(array,'number'));

// let array = [0, 1, false, 2, undefined, '', 3, null , {}]
// function cleaner(arg, type1, type2) {
//   let result = arg.filter(item => (typeof item == type1 || typeof item == type2) && item !== 0 && item !== null)
//     return result
// }

// console.log(cleaner(array, 'number', 'object'));
// // 1. !
// 2. &&
// 3. ||

// asasad && (aasa || asad || asad || (sasad > asafdsf))
//    5        1       2       4        3

// function cleaner(array)
{
  // return array.filter((value) => { return value !== false && value !== undefined && value !== 0 && value !== "" && value !== null; } )
}
// Функція на вхід приймає три параметри:
// масив із чисел, що становить швидкість роботи різних членів команди. Кількість елементів у масиві означає кількість людей у команді. Кожен елемент означає скільки стор поінтів (умовна оцінка складності виконання завдання) може виконати даний розробник за 1 день.
// масив з чисел, який є беклогом (список всіх завдань, які необхідно виконати). Кількість елементів у масиві означає кількість завдань у беклозі. Кожне число в масиві означає кількість сторі поінтів, необхідні виконання даного завдання.
// Дата дедлайну (об’єкт типу Date).
// Після виконання, функція повинна порахувати, чи команда розробників встигне виконати всі завдання з беклогу до настання дедлайну (робота ведеться починаючи з сьогоднішнього дня). Якщо так, вивести на екран повідомлення: Усі завдання будуть успішно виконані за ? днів до настання дедлайну!. Підставити потрібну кількість днів у текст. Якщо ні, вивести повідомлення Команді розробників доведеться витратити додатково ? годин після дедлайну, щоб виконати всі завдання в беклозі
// Робота триває по 8 годин на день по будніх днях

// let points = [2, 5, 8, 10, 6];
// let backlog = [20, 6, 1, 10];
// let data = new Date()
// function calc(points, backlog, data) {

// n2 = n0+n1

// }

// function fibonacchi(n) {
//   if (n < 0 ) {
//   return 0
// }
// if (n <= 2) {
//   return 1
// }
//   return (fibonacchi(n - 1 ) - fibonacchi(n - 2))
// }

// console.log(fibonacchi());

//  1 2 3 4  5  6 7 8 9

// console.log("1 =>", null || 2 || undefined);
// console.log("2 =>", alert(1) || 2 || alert(3));
// console.log("3 =>", 1 && null && 2);
// console.log("4 =>", alert(1) && alert(2));
// console.log("5 =>", null || (2 && 3) || 4);

// /**
//  * Завдання 1.
//  *
//  * Користувач вводить у модальне вікно будь-яке число.
//  *
//  * У консоль вивести повідомлення:
//  * - Якщо число парне, вивести в консоль повідомлення "Ваше число парне."";
//  * - Якщо число не парне, вивести в консоль повідомлення "Ваше число не парне."";
//  * - Якщо користувач ввів не число, вивести нове модальне вікно з повідомленням "".
// //  * - Якщо користувач вдруге запровадив не число, вивести повідомлення: «⛔️ Помилка! Ви ввели не число

// let number = prompt('enter number')
// if (isNaN(number) || number === null || number === "") {
//   number = prompt('Необхідно ввести число!')
// }
// if (isNaN(number) || number === null || number === "") {
//   alert ("⛔️ Помилка! Ви ввели не число")
// } else if (number % 2 === 0) {
//   alert("парне число")
// } else if (number % 2 === 1) { alert("не парне") }

/**
 * Завдання 2.
 *
 * Написати програму, яка вітатиме користувача.
 * Спочатку користувач вводить своє ім'я, після чого програма виводить у консоль повідомлення з урахуванням його посади..
 *
 * Список посад:
 * Mike — CEO;
 * Jane — CTO;
 * Walter — програміст:
 * Oliver — менеджер;
 * John — уборщик.
 *
 * Якщо введено невідоме програмі ім'я, вивести в консоль повідомлення «Користувача не знайдено»..
 *
 * Виконати завдання у двох варіантах:
 * - використовуючи конструкцію if/else if/else;
 * - використовуючи конструкцію switch.
 */

/**
 * Завдання 3.
 *
 * Користувач вводить 3 числа.
 * Вивести в консоль повідомлення з максимальною кількістю з введених.
 *
 * Якщо одне із введених користувачем чисел не є числом,
 * вивести повідомлення: «⛔️ Помилка! Одне з введених чисел не є числом..
 *
 * Умови: об'єктом Math користуватися не можна.
 */

// let first = +prompt("enter first number")
// let second = +prompt("enter second number")
// let third = +prompt("enter third number")
//  if (isNaN(first) ||isNaN(second) ||isNaN(third)) {
//   alert("⛔️ Помилка! Одне з введених чисел не є числом")
//  }

// if (first > second && first > third) {
//   alert(`${first}`);
// } else if (second > first && second > third) {
//   alert(`${second}`)
// } else alert(`${third}`)

// //**
//  * Завдання 4.
//  *
//  * Напишіть програму "Кавова машина".
//  *
//  * Програма приймає монети та готує напої:
//  * - кава за 25 монет;
//  * - Капучіно за 50 монет;
//  * - Чай за 10 монет.
//  *
//  * Щоб програма дізналася, що робити, вона повинна знати:
//  * - Скільки монет користувач вніс;
//  * - Який він бажає напій.
//  *
//  * Залежно від того, який напій вибрав користувач,
//  * програма повинна обчислити здачу та вивести повідомлення в консоль:
//  * «Ваш «НАЗВА НАПОЮ» готовий. Візьміть здачу: "СУМА ЗДАЧІ".".
//  *
//  *Якщо користувач ввів суму без здачі, вивести повідомлення:
//  * «Ваш «НАЗВА НАПОЮ» готовий. Дякую за суму без здачі! :)"//

// let name = prompt('enter the name').toLowerCase()
// let coins = +prompt('enter the value')
// switch (name) {
//   case "кава":
//     if (coins === 25) {
//       alert(`Ваш ${name} готовий. Дякую за суму без здачі!`)
//     } else if (coins > 25) {
//       alert(`Ваш ${name} готовий. Візьміть здачу: ${coins - 25}`)
//     } else alert(`У вас недостатньо коштів`)
//     break;
//   case "капучіно":
//      if (coins === 50) {
//       alert(`Ваш ${name} готовий. Дякую за суму без здачі!`)
//     } else if (coins > 50) {
//       alert(`Ваш ${name} готовий. Візьміть здачу: ${coins - 50}`)
//     } else alert (`У вас недостатньо коштів`)
//     break;
//   case "чай":
//      if (coins === 10) {
//       alert(`Ваш ${name} готовий. Дякую за суму без здачі!`)
//     } else if (coins > 10) {
//       alert(`Ваш ${name} готовий. Візьміть здачу: ${coins - 10}`)
//     } else alert (`У вас недостатньо коштів`)
//   }

/**
 * Завдання 3.
 *
 * Написати функцію-лічильник count.
 *
 * Функцію має два параметри:
 * - Перший - число, з якого необхідно почати рахунок;
 * - Другий - число, яким необхідно закінчити рахунок.
 *
 * Якщо число, з якого починається рахунок більше, ніж число,
 * яким він закінчується, вивести повідомлення:
 * «⛔️ Помилка! Рахунок неможливий.
 *
 * Якщо обидва ці числа однакові, вивести повідомлення:
 * «⛔️ Помилка! Нема чого рахувати.»
 *
 * На початку рахунку необхідно вивести повідомлення:
 * «🏁 Відлік розпочато.».
 *
 * Кожен «крок» рахунку необхідно виводити в консоль.
 * Після проходження останнього кроку необхідно вивести повідомлення:
 * «✅ Відлік завершено.».
 *
 */
// let a = +prompt("first")
// let b = +prompt("second")

// function counter() {
//   if (a > b) {
//     alert("⛔️ Помилка! Рахунок неможливий")
//   } else if (a == b) {
//     alert("⛔️ Помилка! Нема чого рахувати")
//   } else {
//     alert ("🏁 Відлік розпочато")
//     for (let i = a; i <= b; i++) {
//       alert(i);
//     }
//     alert ("✅ Відлік завершено.")
//   }
// }
// counter();

// const tittle = document.querySelectorAll("li")
// for (let node of tittle) {
//   node.innerText = "hello"
//   if (node.classList.contains('world')) {
//     node.innerText += " World"
//   }
//   }
/**
 * Завдання 2.
 *
 * Написати функцію-фабрику квадратів createSquares.
 *
 * Функція має два параметри — кількість квадратів для створення.
 *
 * Якщо користувач ввів кількість квадратів для створення в неприпустимому форматі
 * Запитувати дані повторно до тих пір, поки дані не будуть введені коректно.
 *
 * Максимальна кількість квадратів для створення – 10.
 * Якщо користувач вирішив створити більше 10 квадратів - повідомити йому про неможливість такої операції
 * і запитувати дані про кількість квадратів для створення доти, доки вони не будуть введені коректно.
 *
 * Розмір кожного квадрата в пікселях потрібно отримати інтерактивно за допомогою діалогового вікна prompt.
 *
 * Якщо користувач ввів розмір квадрата в неприпустимому форматі
 * Запитувати дані повторно до тих пір, поки дані не будуть введені коректно.
 *
 * Колір кожного квадрата необхідно запитати після введення розміру квадрата у коректному вигляді.
 *
 * Разом послідовність введення даних про квадрати виглядає так:
 * - розмір квадрата n;
 * - колір квадрата n;
 * - розмір квадрата n + 1;
 * - Колір квадрата n+1.
 * - розмір квадрата n + 2;
 * - колір квадрата n + 3;
 * - і так далі...
 *
 * Якщо не будь-якому етапі збору даних про квадрати користувач натиснув на кнопку «Скасувати»,
 * необхідно зупинити процес створення квадратів та вивести в консоль повідомлення:
 * «Операція перервана користувачем.».
 *
 * Всі стилі для кожного квадрата встановити через JavaScript за раз.
 *
 * Тип елемента, що описує кожен квадрат - div.
 * Задати новоствореним елементам CSS-класи: .square-1, .square-2, .square-3 і так далі.
 *
 * Усі квадрати необхідно зробити нащадками body документа.
 */

// let quantity = +prompt("Enter some value");
// while (quantity > 10) {
//   quantity = +prompt(" Enter correct value")
// }
// while (isNaN(quantity) || quantity === "" || quantity === null) {
//   quantity = +prompt(" Enter correct value");
// }

// const createSquares = function (quantity) {
//   for (let i = 0; i < quantity; i++) {
//     let size = prompt("Enter some value");
//     if (size === null) {
//       console.log("Операція перервана користувачем");
//       break;
//     }
//     while (isNaN(size) || size === "") {
//       size = +prompt("Enter correct value");
//     }

//     const square = document.createElement("div");
//     square.classList.add(`square-${i + 1}`);
//     square.style.width = `${size}px`;
//     square.style.height = `${size}px`;
//     square.style.backgroundColor = "red";
//     document.body.append(square);
//   }
// };
// createSquares();

/**
 * Завдання 6.
 *
 * Написати функцію-лічильник increment.
 *
 * Перший виклик функції повинен повернути 0.
 * Кожен новий виклик функції повинен повертати число, що на 1 більше, ніж попереднє.
 *
 * Просунута складність:
 * - технічно докладно пояснити механізм рішення.
 */

// function increment() {
//   let result = 0;
  

  
//   return result
// }
// console.log(increment());

// const number = prompt("")
//   function nIncrement () {
//     let count = 0;
//      return function second() {
//             return count++
//     };
//    }
//     const n = nIncrement();
//   for (let i = 0; i < number; i++){
//   console.log(n()); }
  
/**
 * Завдання 1.
 *
 * Напишіть код, щоб зафарбувати всі діагональні клітинки таблиці червоним кольором.
 *
 */


// const table = document.getElementsByTagName('table')[0];
// console.log(table.rows);
// let rowIndex = 0;
// let rowReverseIndex = table.rows[0].cells.length - 1;
// let rowMiddleIndex = Math.floor(table.rows[0].cells.length / 2)
// for (let node of table.rows) {
//   node.cells[rowIndex].style.backgroundColor = 'red';
//     node.cells[rowReverseIndex].style.backgroundColor = 'red';
//    if (rowIndex == rowMiddleIndex && rowReverseIndex == rowMiddleIndex) {
//      node.cells[rowMiddleIndex].style.backgroundColor = 'green' ;
//   }
//     rowIndex++
//     rowReverseIndex--
 
// }


/**
 * При натисканні на кнопку Validate відображати
 * VALID зеленим кольром, якщо значення проходить валідацію
 * INVALID червоним кольором, якщо значення не проходить валідацію
 *
 * Правила валідації значення:
 * - значення не пусте
 *
 * ADVANCED
 * Правила валідації значення:
 * - повинно містити щонайменше 5 символів
 * - не повинно містити пробілів
 * - повинно починатися з літери (потрібно використати регулярні вирази)
 *
 */

// const input = document.querySelector('#input')
// const button = document.querySelector('button')
// const text = document.createElement('p')

// button.addEventListener('click', () => {
//   if (input.value === '') {
//     text.innerText = 'INVALID'
//     text.style.color = 'red'
//     } else {text.innerText = 'VALID'
//     text.style.color = 'green'
//   }
//   document.body.append(text)
// })

/*
 * Завдання 5.
 *
 *  Запитати в користувача ім'я, прізвище, вік (перевірити на правильну цифру
 * і що число між 15 і 100), чи є діти, якого кольору очі.
 * Усі ці дані записати в об'єкт.
 * Перебрати об'єкт і вивести у консолі фразу
 * `Властивість ${name}: ${значення name}` і так з усіма властивостями
 */

// let userAge = prompt('Введіть ваш вік')
// while (isNaN(userAge) || userAge <= 15 || userAge >= 100 || userAge === 'null' || userAge === "") {
//  userAge = prompt('Введіть ваш вік')
// }


// let newUser = {
//   age: userAge,
//   name: prompt("Введіть ваше ім'я"),
//   surname : prompt ('Введіть ваще прізвище')
// }
// console.log(newUser);

// for (let key in newUser) {
//   console.log(`Властивість ${key} значення ${newUser[key]}`);
//  }

// **
//  * Завдання 6.
//  *
//  * За допомогою циклу for...in вивести в консоль усі властивості
//  * першого рівня об'єкта у форматі «ключ-значення».
//  *
//  * Просунута складність:
//  * Поліпшити цикл так, щоб він умів виводити властивості об'єкта другого рівня вкладеності.
//  */

/* Дано */
// const user = {
//   firstName: "Walter",
//   lastName: "White",
//   job: "Programmer",
//   pets: {
//     cat: "Kitty",
//     dog: "Doggy",
//   },
// };


// let value = Object.values(user)
// console.log(value[3]);

/**
 * Завдання 7.
 *
 * Написати функцію-помічник для ресторану.
 *
 * Функція має два параметри:
 * - Розмір замовлення (small, medium, large);
 * - Тип обіду (breakfast, lunch, dinner).
 *
 * Функція повертає об'єкт із двома полями:
 * - totalPrice - загальна сума страви з урахуванням її розміру та типу;
 * - totalCalories — кількість калорій, що міститься у блюді з урахуванням його розміру та типу.
 *
 * Нотатки:
 * - Додаткові перевірки робити не потрібно;
 * - У рішенні використовувати референтний об'єкт з цінами та калоріями.
 */

// /* Дано */
// const priceList = {
//   sizes: {
//     small: {
//       price: 15,
//       calories: 250,
//     },
//     medium: {
//       price: 25,
//       calories: 340,
//     },
//     large: {
//       price: 35,
//       calories: 440,
//     },
//   },
//   types: {
//     breakfast: {
//       price: 4,
//       calories: 25,
//     },
//     lunch: {
//       price: 5,
//       calories: 5,
//     },
//     dinner: {
//       price: 10,
//       calories: 50,
//     },
//   },
// };


// function helper(type, size) {
//   const object = {
//     totalPrice: priceList.sizes[size].price + priceList.types[type].price,
//     totalCalories: priceList.sizes[size].calories + priceList.types[type].calories,

//   }
//    return object
// }
 

// console.log(helper('lunch','large'));


// Додати у Html кнопку ScrollToTop.
// За дефолтом кнопку не видно.
// Коли користувач проскролить більше, ніж висота вікна (window.innerHeight) - показати кнопку.
// Кнопка має бути фіксована внизу сторінки.
// По кліку неї - треба проскролить на початок документа.
// (https://developer.mozilla.org/ru/docs/Web/API/Window/scrollTo)

// const button = document.createElement('div')
// button.innerText = 'ScrollToTop'
// button.classList.add("button")
// button.classList.add("button_in_active")
// document.body.append(button)

// window.addEventListener("scroll", (event) => {
//   console.log(window.pageYOffset);
//   if (window.pageYOffset > 1000) {
//     button.classList.remove("button_in_active")
//   } else if (window.pageYOffset < 1000) {
//     button.classList.add("button_in_active")
//   }
// })

// button.addEventListener('click', () => {
//   window.scrollTo({
//     top: 0,
//     behavior: "smooth"
//   })
// })





/**
 * Завдання.
 *
 * Необхідно "оживити" навігаційне меню за допомогою JavaScript.
 *
 * При натисканні на елемент меню додавати до нього CSS-клас .active.
 * Якщо такий клас вже існує на іншому елементі меню, необхідно
 * з того, що попереднього елемента CSS-клас .active зняти.
 *
 * Кожен елемент меню — це посилання, що веде на google.
 * За допомогою JavaScript необхідно запобігти перехід по всіх посилання на цей зовнішній ресурс.
 *
 * Умови:
 * - У реалізації обов'язково використовувати прийом делегування подій (на весь скрипт слухач має бути один).
 */



// const list = document.getElementById("navList");
// let activeElement = null;
// list.addEventListener('click', (event) => {
//   event.preventDefault()
//   if (activeElement) {
//     activeElement.classList.remove('active')
//   }
//   event.target.classList.add('active')
//   activeElement = event.target
//     })

