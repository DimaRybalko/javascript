// 1. метод forEach перебирає весь масив і до кожного елемента масиву застосовує callBack функцію
// 2. щоб очистити масив можна задати його довжину як 0
// 3.  Щоб перевірити чи є елемент масивом можна застосувати перевірку isArray


function filterBy(array, type) {
    let newArray = []
    
    for (let element of array) {

        if (typeof element !== type) {

            newArray.push(element)
                   }
    }
return newArray
}

let arr = ['hello', 'world', 23, '23', null]
console.log(filterBy(arr, 'string'));

// Через методи масивів

function filterBy(array, type) {
  return array.filter(element => typeof element !== type);
}

let result = ['hello', 'world', 23, '23', null, undefined];
console.log(filteredData(result, 'string'));