const button = document.querySelector('button')
button.addEventListener('click', () => {
const upper = document.querySelector('.first').value
const lower = document.querySelector('.second').value
    if (upper == '' || lower == '') {
        alert("Потрібно ввести значення в кожне поле")
    } else if (upper === lower) {
        alert("You are welcome")
    } else {
        const fail = document.createElement('div')
        fail.innerText = 'Потрібно ввести однакові значення'
        fail.style.cssText ='color: red; margin-top: -45px'      
        document.body.append(fail)
    }
})

const passSwitcher = document.querySelectorAll('.fas')
passSwitcher.forEach((element) => {
    element.addEventListener('click',() => {
        let target = element.getAttribute('data-target')
        let password = document.querySelector(target)
        
        if (password.getAttribute('type') === 'password') {
            password.setAttribute('type', 'text')
            element.classList.add('fa-eye-slash')
        } else {
            password.setAttribute('type', 'password')
            element.classList.remove('fa-eye-slash')
        }
    })
} )