const switcher = document.createElement("div");
switcher.innerText = "Змінити тему";
switcher.style.cssText =
  " width: 150px; text-align:center; color: red; border: 1px solid red ;margin-top: -1200px; margin-left: 1200px";
document.body.append(switcher);
console.log(switcher);

switcher.addEventListener("click", (event) => {
  if (localStorage.getItem("theme") === "dark") {
    localStorage.removeItem("theme");
  } else {
    localStorage.setItem("theme", "dark");
  }
  darkTheme();
});

function darkTheme() {
  if (localStorage.getItem("theme") === "dark") {
    document.querySelector("html").classList.add("dark");
  } else {
    document.querySelector("html").classList.remove("dark");
  }
}
darkTheme();
