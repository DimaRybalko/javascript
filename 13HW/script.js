// 1. Різниця між цимим функціями у тому що перша виклкається один раз , а друга буде постійно
//  викликатись через певний ітервал часу
// 2. Якщо задати нульову затримку , то дана функція виконається якомога швидше, але не моментально
// через те що у JS є черга подій
// 3. Тому що setInterval буде виконуватись нескінченну кількість разів, що буде перенавантажувати систему.

let begining = 0;
function slider() {
  const images = document.querySelectorAll(".image-to-show");
  for (let i = 0; i < images.length; i++) {
    images[i].style.display = "none";
  }
  begining++;
  if (begining > images.length) {
    begining = 1;
  }
  images[begining - 1].style.display = "block";
}
slider();
let timer = setInterval(slider, 3000);

const cancel = document.createElement("div");
cancel.innerText = "Припинити";
cancel.style.cssText =
  "background-color: red; width:80px; height:20px;margin-left:600px ; margin-top:30px; cursor:pointer; display:none";
document.body.append(cancel);

const proceed = document.createElement("div");
proceed.innerText = "Продовжити";
proceed.style.cssText =
  "background-color: green; width:87px; height:20px;margin-left:750px ; margin-top: -20px; cursor:pointer; display:none";
document.body.append(proceed);

setTimeout(() => {
  cancel.style.display = "block";
  proceed.style.display = "block";
}, 3000);

cancel.addEventListener("click", () => {
  clearInterval(timer);
  cancel.setAttribute("disabled", "");
  proceed.removeAttribute("disabled");
});

proceed.addEventListener("click", () => {
  timer = setInterval(slider, 3000);
  proceed.setAttribute("disabled", "");
  cancel.removeAttribute("disabled");
});
