// 1 завдання
// 1) Цикли використовуються для того щоб упростити задачу повторення однієї і тієї ж дії кілька разів підряд.
// 2) цикл while використовується для того щоб дія виконувалась доки умова буде виконуватись, цикл фор використовується для
// повторення однієї і тієї ж дії , цикл do .. while використовується для того щоб одна дія виконалась не залежно від того чи
// відповідає вона заданій умові чи ні.
// 3) Неявне перетворення на відміну від явного, це перетворення типів даних, яке проводиться системою самостійно для зручності роботи з ними.

// 2 завдання

let value = prompt("Enter some value")
while (isNaN(value) || value === null || value === "" || Number.isInteger(value)) {
    value = prompt("Enter some value")
}

if (value < 5) { console.log("Sorry, no numbers") }
else {
    for (let i = 5; i <= value; i++) {
        if (i % 5 === 0)
            console.log(i);
    }
}

// Підвищенна складність  ( поки що не виходить зробити)
// let m = prompt("enter lower number")
// while (isNaN(m) || Number.isInteger(m) || m === null || m == '') {
//     m = prompt ("enter lower number")
// }



// let n = prompt("enter higher number")
// while (isNaN(n) || Number.isInteger(n) || n === null || n == '') {
//     n = prompt("enter higher number")
// }

// if (m < n) {
//     if (m < 2) {
//         m = 2
//  }
// }

// for (let i = m; i <= n; i++) {
//     for (let j = 2; j < i; j++) {
//         if (i % j == 0) continue
//     }
//     console.log(i);
// }