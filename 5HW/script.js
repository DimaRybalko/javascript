// 1. Методами об'єктів називаються функції які вже є всередині об'єкта і які можна виконвувати з об'єктами
// 2. Об'єкт може приймати за значення будь які типи даних.
// 3. Посилальний тип даних це той тип даних в якому міститься лише ссилка на данні 


function createNewUser() {
    let newUser = {};
  newUser.firstName = prompt("Enter your name");
    newUser.lastName = prompt("Enter your surname");
    newUser.birthday = prompt("Enter your birthday data(dd.mm.yyyy)")

    newUser.getLogin = function() {
             return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }
    
    newUser.getAge = function () {
                let birth = new Date(this.birthday.split('.').reverse().join('.'));
        let years = new Date().getFullYear() - birth.getFullYear() 
        let months = new Date().getMonth() - birth.getMonth()
        let days = new Date().getDate() - birth.getDate()
        if (months < 0 || months === 0 && days < 0) {
            return years - 1 
        } 
        
    } 

    newUser.getPassword = function () {
        return  this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split(".")[2]
    }
    return newUser
}

let user = createNewUser()
console.log(user);
let login = user.getLogin ()
console.log(login);
let date = user.getAge();
console.log(date);
let password = user.getPassword()
console.log(password);
